using Godot;
using System;

public class QuestArea : Area2D
{
    [Export] public string QuestID;
    [Export] public string QuestDisplayName;
    [Export] public string RequiredNotes;
    [Export] public bool IsComplete = false;

    public Timer CheckTimer;
    public override void _Ready()
    {
        CheckTimer = this.GetNode<Timer>("CheckTimer");
        CheckTimer.Connect("timeout", this, "CheckNotes");
        this.Connect("body_entered", this, "OnBodyEntered");
        this.Connect("body_exited", this, "OnBodyExited");
    }

    public void OnBodyEntered(Node body)
    {
        if (body.Name == "Player" && !IsComplete)
        {
            Utils.globalPlayer.XyloNotes = "";
            CheckTimer.Start(0.5f);
        }
    }

    public void OnBodyExited(Node body)
    {
        if (body.Name == "Player" && !IsComplete)
        {
            Utils.globalPlayer.XyloNotes = "";
            CheckTimer.Stop();
        }
    }

    public void CheckNotes()
    {
        GD.Print(QuestID + " " + RequiredNotes + " " + Utils.globalPlayer.XyloNotes);
        if (Utils.globalPlayer.XyloNotes == RequiredNotes)
        {
            //Quest has been completed
            GD.Print("You have completed the quest! " + QuestID);
            Utils.globalPlayer.XyloNotes = "";
            Utils.globalPlayer.ShowToast(QuestDisplayName);
            CheckTimer.Stop();
            this.IsComplete = true;
            PackedScene finishquest = GD.Load<PackedScene>("res://Scenes/ui/ConfettiExplode.tscn");
            this.AddChild(finishquest.Instance());
            Utils.UpdateQuestCount();
            if (this.GetParent().GetMeta("NPC") != null)
            {
                NPC parent = this.GetParent() as NPC;
                parent.IsComplete = true;
                parent.UpdateDialog(parent.questCompleteMessage);
                
            }
            if (Utils.QuestIDRegister.Contains(this.QuestID))
            {
               (Utils.QuestIDRegister[this.QuestID] as QuestTrigger).Trigger();
            }

        }

    }


    public override void _Process(float delta)
    {

    }
}
