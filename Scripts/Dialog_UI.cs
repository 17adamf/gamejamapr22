using Godot;
using System;

public class Dialog_UI : Control
{

    public string sender { get; set; }
    public string[] message;
    public Godot.Collections.Array options { get; set; }

    public int currentMessageIndex = 0;

    public RichTextLabel dialogOutputBox;
    /*
    option format
        {
            {"Option Text", next text node to load},
            {"Option Text", next text node to load},
            {"Option Text", next text node to load},
            {"Option Text", next text node to load},
        }
    */


    private string[] keygen = { "[k]", "[/k]", "[wave amp=30 freq=5][rainbow freq=.2 sat=1 val=1]", "[/rainbow][/wave]" };
    private string[] anger = { "[a]", "[/a]", "[shake rate=100 level=15][color=red]", "[/color][/shake]" };

    public override void _Ready()
    {
        dialogOutputBox = this.GetNode<RichTextLabel>("PanelContainer/HBoxContainer/vboxcontent/Content/RichTextLabel");
        this.GetParent().MoveChild(this, this.GetParent().GetChildCount());
        //default options to set
        options = new Godot.Collections.Array(){
            new Godot.Collections.Array(){
                "Continue...", "Advance the Dialog"
            }
        };



        this.GetNode<Button>("PanelContainer/HBoxContainer/vboxoptions/Exit").Connect("pressed", this, "CloseDialog");

        //set sender text
        this.GetNode<Label>("PanelContainer/HBoxContainer/vboxcontent/TopBar/HBoxContainer/Sender").Text = sender + " Says:";

        //set options text
        int i = 1;
        foreach (Godot.Collections.Array option in options)
        {
            Button optionbtn = this.GetNode<Button>("PanelContainer/HBoxContainer/vboxoptions/Dialog" + i);
            optionbtn.Visible = true;
            optionbtn.Text = option[0].ToString();
            optionbtn.HintTooltip = option[1].ToString();
            optionbtn.Connect("pressed", this, "NextDialog");
            i++;
        }



        //set dialog box text
        string formatted = FormatText(message[currentMessageIndex]);
        dialogOutputBox.PercentVisible=0f;
        dialogOutputBox.BbcodeText = formatted;

    }
    private void NextDialog()
    {
        currentMessageIndex++;
        if (currentMessageIndex >= message.Length)
        {
            CloseDialog();
        }
        else
        {
            string formatted = FormatText(message[currentMessageIndex]);
            dialogOutputBox.PercentVisible=0f;
            dialogOutputBox.BbcodeText = formatted;
        }

    }
    private string FormatText(string text)
    {
        string formatted = "";
        string[] words = text.Split(' ');
        foreach (string word in words)
        {
            string temp = word;
            if (word == keygen[0])
            {
                temp = keygen[2];
            }
            if (word == keygen[1])
            {
                temp = keygen[3];
            }
            if (word == anger[0])
            {
                temp = anger[2];
            }
            if (word == anger[1])
            {
                temp = anger[3];
            }
            formatted += temp + " ";
        }

        return formatted;
    }
    private void CloseDialog()
    {
        // Input.SetMouseMode(Input.MouseMode.Captured);
        Utils.CurrentSceenRoot.GetTree().Paused = false;
        Utils.KillNode(this);
    }
    public override void _PhysicsProcess(float delta)
    {

        if (dialogOutputBox.PercentVisible < 1.0f)
        {
            dialogOutputBox.PercentVisible += delta;
        }
    }
}
