using Godot;
using System;

public static class Utils
{
    public static SceneManager CurrentSceenRoot { get; set; }
    public static bool isPaused = false;//true means game is paused

    public static Player globalPlayer;
    public static Godot.Collections.Dictionary QuestIDRegister = new Godot.Collections.Dictionary();
    public static QuestTrigger quest;
    public static int MinQuestCount = 7;
    public enum TriggerType
    {
        DestroyNode,
        AppearNode,
        SpawnNode,

    }
    public static void RegisterQuest(string questID, QuestTrigger questTrigger)
    {
        QuestIDRegister.Add(questID, questTrigger);
    }
    //implementation of the clamp function. For whatever reason this didn't exist by default in godot mono?
    public static T Clamp<T>(this T val, T min, T max) where T : IComparable<T>
    {
        if (val.CompareTo(min) < 0) return min;
        else if (val.CompareTo(max) > 0) return max;
        else return val;
    }

    public static void TogglePause()
    {
        isPaused = !isPaused;
    }
    public static void CloseUI()
    {
        CurrentSceenRoot.GetTree().Paused = false;
        Input.SetMouseMode(Input.MouseMode.Captured);
        //assume UIs will close themselves in the correct way.
    }
    //loads a new scene/map
    public static void LoadMap(string path)
    {

        CurrentSceenRoot.QueueFree();
        CurrentSceenRoot.GetTree().ChangeScene(path);

        //TODO: figure out multithreading loading of scenes 

    }
    public static void PlaySoundOneshot(string soundname, Node parent, Vector2 location)
    {
        AudioStreamPlayer2D audio = new AudioStreamPlayer2D()
        {
            Stream = (AudioStreamOGGVorbis)ResourceLoader.Load(soundname),
            Autoplay = true,
            Position = location,
        };

        audio.Connect("finished", CurrentSceenRoot, "KillNode", new Godot.Collections.Array { audio });
        parent.AddChild(audio);
    }
    public static void UpdateQuestCount()
    {
        globalPlayer.QuestCounter++;
        if (globalPlayer.QuestCounter >= MinQuestCount)
        {
            (Utils.QuestIDRegister["Ending"] as QuestTrigger).Trigger();
            Timer timer = new Timer()
            {
                WaitTime = 7,
                OneShot = true,
                Autostart = true
            };
            timer.Connect("timeout", globalPlayer, "EndingToast");
            globalPlayer.AddChild(timer);
        }
    }
    private static float LerpF(float firstFloat, float secondFloat, float by)
    {
        return firstFloat * (1 - by) + secondFloat * by;
    }
    public static Vector2 Lerp(Vector2 firstVector, Vector2 secondVector, float by)
    {
        float retX = LerpF(firstVector.x, secondVector.x, by);
        float retY = LerpF(firstVector.y, secondVector.y, by);
        return new Vector2(retX, retY);
    }
    public static void ShowDialog(string sender, string[] message)
    {
        PackedScene _dialog = (PackedScene)ResourceLoader.Load("res://Scenes/ui/Dialog_UI.tscn");
        Dialog_UI dialog = (Dialog_UI)_dialog.Instance();
        dialog.message = message;
        dialog.sender = sender;
        dialog.options = null;
        // Input.SetMouseMode(Input.MouseMode.Visible);
        CurrentSceenRoot.GetTree().Paused = true;
        dialog.RectPosition = globalPlayer.Position;
        CurrentSceenRoot.AddChild(dialog);
    }


    //kills the node passed to it
    public static void KillNode(Node node) => node.QueueFree();

    //exits the game gracefully
    public static void Exit()
    {
        CurrentSceenRoot.GetTree().Quit();
    }
}
