using Godot;
using System;

public class NPC : AnimatedSprite
{
    [Export] public string npcname;
    [Export] public string[] message;

    [Export] public SpriteFrames characterSprites;
    [Export] public string AnimName;

    //Quest Related Exports
    [Export] public bool hasQuest = false;
    [Export] public string[] questCompleteMessage;
    [Export] public string QuestID;
    [Export] public string QuestDisplayName;
    [Export] public string RequiredNotes;
    [Export] public bool IsComplete = false;



    public Interactable npcDialog;
    public QuestArea npcQuest;
    public override void _Ready()
    {
        this.SetMeta("NPC", true);
        if(characterSprites != null)
            this.Frames = characterSprites;
        if(AnimName != null)
            this.Animation = AnimName;
        
        this.npcDialog = this.GetNode<Interactable>("InteractableItem");
        this.npcQuest = this.GetNode<QuestArea>("QuestArea");
        //setup dialog
        this.npcDialog.name = this.npcname;
        this.npcDialog.message = this.message;
        //setup quest
        if (this.hasQuest)
        {
            this.npcQuest.QuestID = this.QuestID;
            this.npcQuest.QuestDisplayName = this.QuestDisplayName;
            this.npcQuest.RequiredNotes = this.RequiredNotes;
            this.npcQuest.IsComplete = this.IsComplete;
        }
    }
    public void UpdateDialog(string[] newMessage)
    {
        this.npcDialog.message = newMessage;
    }

}
