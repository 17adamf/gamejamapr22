using Godot;
using System;

public class QuestTrigger : Sprite
{
    [Export] public string QuestID;
    [Export] public Utils.TriggerType CompleteTrigger;
    [Export] public PackedScene NodeToLoad;
    public override void _Ready()
    {
        Utils.RegisterQuest(QuestID, this);
        if(this.CompleteTrigger == Utils.TriggerType.AppearNode){
            this.Visible = false;
        }
    }

    public void Trigger(){
        switch(this.CompleteTrigger){
            case Utils.TriggerType.DestroyNode:
                this.QueueFree();
                break;
            case Utils.TriggerType.AppearNode:
                this.Visible = true;
                break;
            case Utils.TriggerType.SpawnNode:
                Node2D _NodeToLoad = NodeToLoad.Instance() as Node2D;
                _NodeToLoad.Transform = this.Transform;
                this.GetParent().AddChild(_NodeToLoad);
                break;
        }
    }
}
