using Godot;
using System;

public class ItemCollisionEvent : Node2D
{
    public enum itemEventType
    {
        AddXylo,
    }
    [Export] public itemEventType eventType;
    [Export] public Sprite itemSprite;
    public override void _Ready()
    {
        this.GetNode<Area2D>("ItemPickup/Collide").Connect("body_entered", this, "ItemPickup");
        this.GetNode<AnimationPlayer>("ItemPickup/bobanim").CurrentAnimation = "bob";
        if (itemSprite != null)
        {
            this.GetNode<Sprite>("ItemPickup").Texture = itemSprite.Texture;
        }
    }

    public void ItemPickup(Node body)
    {
        if (body.Name == "Player")
        {

            switch (this.eventType)
            {
                case itemEventType.AddXylo:
                    Utils.globalPlayer.hasXylo = true;
                    Utils.globalPlayer.ShowToast("Xylophone Acquired!");
                    break;
                default:
                    GD.Print("No valid item");
                    break;
            }
            Utils.KillNode(this);
        }
    }
}
