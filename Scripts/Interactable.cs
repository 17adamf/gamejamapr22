using Godot;
using System;

public class Interactable : AnimatedSprite
{
    [Export] public string name;
    [Export] public string[] message;
    Area2D radius;
    public override void _Ready()
    {
        radius = this.GetNode<Area2D>("Area2D");
        radius.Connect("body_entered", this, "OnBodyEntered");
        radius.Connect("body_exited", this, "OnBodyExited");
        this.Animation = "close";
        this.Frame = 5;
    }
    public void OnInteract()
    {
        Utils.ShowDialog(name, message);
        
    }
    public void OnBodyEntered(Node body)
    {
        if (body.Name == "Player")
        {
            this.Animation = "open";
            Utils.globalPlayer.closestInteractable = this;
        }
    }
    public void OnBodyExited(Node body)
    {
        if (body.Name == "Player")
        {
            this.Animation = "close";
            Utils.globalPlayer.closestInteractable = null;
        }
    }
}
