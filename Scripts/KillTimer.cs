using Godot;
using System;

public class KillTimer : Timer
{

    public override void _Ready()
    {
        this.Connect("timeout", this, "KillNode");
    }
    public void KillNode() => Utils.KillNode(this.GetParent());
}
