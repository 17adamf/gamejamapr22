using Godot;
using System;
public class Player : KinematicBody2D
{
    [Export] public int speed = 200;
    [Export] public bool hasXylo = false;
    [Export] public bool requireIntro = false;
    [Export] public int QuestCounter = 0;

    public bool isIntroPlaying = false;

    public Vector2 velocity = new Vector2();
    public AnimatedSprite sprite;
    public bool isSleeping = false;
    public Camera2D cam;

    public float zoomX;
    public float zoomY;
    public Interactable closestInteractable;

    public bool isInstrumentOpen = false;

    public string XyloNotes = "";
    private string lastAnimation = "";

    Timer idletimer = new Timer()
    {
        OneShot = true,
    };
    public float timeoutTime = 10.0f;
    public override void _Ready()
    {
        sprite = this.GetNode<AnimatedSprite>("Sprite");
        idletimer.Connect("timeout", this, "IdleTimeout");
        this.AddChild(idletimer);
        idletimer.Start(timeoutTime);
        cam = this.GetNode<Camera2D>("Camera2D");
        Utils.globalPlayer = this;
        //start the intro sequence
        if (requireIntro)
        {
            this.ZIndex = 10;
            isIntroPlaying = true;
            this.GetNode<ColorRect>("IntroFade").Visible = true;
            this.GetNode<AnimationPlayer>("IntroFade/Intro").Play("FadeIn");
            Timer introtimer = new Timer()
            {
                Autostart = true,
                WaitTime = 3f,
                OneShot = true
            };
            introtimer.Connect("timeout", this, "EndIntro");
            this.AddChild(introtimer);
            sprite.Animation = "wakeup";

        }

    }
    public void EndIntro()
    {
        this.ZIndex = 0;
        isIntroPlaying = false;
        sprite.Animation = "default";
    }
    public void ShowToast(string message)
    {
        this.GetNode<RichTextLabel>("Camera2D/Textbg/Toast").BbcodeText =
        "[center] [wave amp=5] [color=#045100] \nQuest Complete!\n" +
        message
        + "\n [/color] [/wave] [/center]";
        this.GetNode<AnimationPlayer>("Camera2D/Textbg/Slide").Play("slidedown");
        Utils.PlaySoundOneshot("res://Assets/audio/questcomplete.ogg", Utils.CurrentSceenRoot, Utils.globalPlayer.Position);
    }
    public void EndingToast()
    {
        this.GetNode<RichTextLabel>("Camera2D/Textbg/Toast").BbcodeText =
        "[center] [wave amp=5] [color=#045100] \nReturn Home\nTo The Forest\n [/color] [/wave] [/center]";
        this.GetNode<AnimationPlayer>("Camera2D/Textbg/Slide").Play("slidedown");
        Utils.PlaySoundOneshot("res://Assets/audio/questcomplete.ogg", Utils.CurrentSceenRoot, Utils.globalPlayer.Position);
    }

    public void IdleTimeout()
    {
        lastAnimation = "sleep";
        isSleeping = true;
    }
    public void GetInput()
    {
        if (!Utils.isPaused && !isIntroPlaying)
        {


            velocity = new Vector2();

            if (Input.IsActionJustPressed("togglexylo") && !isInstrumentOpen && hasXylo)
            {
                PackedScene xyloUI = GD.Load<PackedScene>("res://Scenes/ui/InstrumentUI.tscn");
                Control instUI = xyloUI.Instance() as Control;
                instUI.RectPosition = Utils.globalPlayer.Position;
                Utils.CurrentSceenRoot.AddChild(instUI);
                isInstrumentOpen = true;
            }
            if (Input.IsActionJustPressed("interact"))
            {
                if (closestInteractable != null)
                {
                    closestInteractable.OnInteract();
                }
            }
            if (Input.IsActionPressed("right"))
            {
                velocity.x += 1;
                lastAnimation = "walkright";
                isSleeping = false;
                idletimer.Start(timeoutTime);
            }

            if (Input.IsActionPressed("left"))
            {
                velocity.x -= 1;
                lastAnimation = "walkleft";
                isSleeping = false;
                idletimer.Start(timeoutTime);
            }

            if (Input.IsActionPressed("down"))
            {

                velocity.y += 1;
                lastAnimation = "walkdown";
                isSleeping = false;
                idletimer.Start(timeoutTime);
            }

            if (Input.IsActionPressed("up"))
            {

                velocity.y -= 1;
                lastAnimation = "walkup";
                isSleeping = false;
                idletimer.Start(timeoutTime);
            }

            if (!Input.IsActionPressed("down") && !Input.IsActionPressed("up") && !Input.IsActionPressed("left") && !Input.IsActionPressed("right") && !isSleeping)

            {
                sprite.Frame = 0;
                sprite.Playing = false;
            }
            else
            {
                sprite.Animation = lastAnimation;
                sprite.Playing = true;
            }



            velocity = velocity.Normalized() * speed;
        }
        else
        {
            velocity = new Vector2(0, 0);
            if (!isIntroPlaying)
            {

                sprite.Animation = "default";
            }
        }
    }
    
    public override void _PhysicsProcess(float delta)
    {

        GetInput();
        velocity = MoveAndSlide(velocity);
    }
}