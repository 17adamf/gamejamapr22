using Godot;
using System;

public class SceneManager : Node2D
{
  
    public override void _Ready()
    {
        Utils.CurrentSceenRoot = this;
        OS.WindowMaximized = true;
    }

  
    public override void _Process(float delta)
    {

    }
    public void KillNode(Node node) => Utils.KillNode(node);
}
