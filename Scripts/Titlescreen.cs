using Godot;
using System;

public class Titlescreen : Control
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        this.GetNode<Button>("PlayBtn").Connect("pressed", this, "StartLoad");
        this.GetNode<AnimationPlayer>("Fade/FadeAnim").Connect("animation_finished", this, "Load");
         OS.WindowMaximized = true;
    }

    public void StartLoad(){
        this.GetNode<AnimationPlayer>("Fade/FadeAnim").Play("FadeOut");
    }
    public void Load(string str){
        GetTree().ChangeScene("res://Scenes/Main.tscn");
    }

}
