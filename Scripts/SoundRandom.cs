using Godot;
using System;

public class SoundRandom : AudioStreamPlayer2D
{
    [Export] float PitchRandRange = 0.0f;
    [Export] bool RandomPitch = false;
    [Export] int FrequencyInSeconds = 1;
    [Export] int FrequencyRandomRange = 0;

    public Timer randomtimer;
    public override void _Ready()
    {
        this.Connect("finished", this, "StopPlay");
        randomtimer = new Timer()
        {

        };
        randomtimer.Connect("timeout", this, "Fire");
        this.AddChild(randomtimer);
        randomtimer.Start(FrequencyInSeconds + (float)GD.RandRange(-FrequencyRandomRange, FrequencyRandomRange));
    }

    public void StopPlay()
    {
        this.Playing = false;
        this.Autoplay = false;
        randomtimer.Start(FrequencyInSeconds + (float)GD.RandRange(-FrequencyRandomRange, FrequencyRandomRange));
    }
    public void Fire()
    {
        if (RandomPitch)
        {
            this.PitchScale = 1 + (float)GD.RandRange(-PitchRandRange, PitchRandRange);

        }
        this.Play();
    }
}
