using Godot;
using System;

public class InstrumentUI : Control
{
    public TextureRect xylo;
    public TextureRect stick;
    public int currentHover = -1;
    public Node currentBody;

    public Vector2 offset = new Vector2(-15, -15);
    public override void _Ready()
    {
        xylo = this.GetNode<TextureRect>("Xylo");
        stick = this.GetNode<TextureRect>("Xylo_Stick_Sprite");
        stick.GetNode<Area2D>("Area2D").Connect("body_entered", this, "OnBodyEntered");
        Utils.TogglePause();
        this.GetParent().MoveChild(this, this.GetParent().GetChildCount());

        Utils.globalPlayer.XyloNotes = "";
        
    }
    public void OnBodyExited(Node body)
    {
        currentHover = -1;
    }
    public void OnBodyEntered(Node body)
    {
        currentHover = body.GetIndex();
        currentBody = body;

    }
    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        //get current mouse potition
        Vector2 mousePos = GetLocalMousePosition();
        mousePos += offset;
        //lerp xylostick to mouse position
        stick.RectPosition = Utils.Lerp(stick.RectPosition, mousePos,1f);
        if (Input.IsActionJustPressed("leftclick"))
        {

            if (currentHover != -1)
            {

                Utils.PlaySoundOneshot("res://Assets/audio/xylo" + currentHover + ".ogg", Utils.CurrentSceenRoot, Utils.globalPlayer.Position);
                PackedScene poof = GD.Load<PackedScene>("res://Scenes/ui/Poof.tscn");
                Particles2D poofInstance = poof.Instance() as Particles2D;
                poofInstance.Position = GetLocalMousePosition();
                poofInstance.Emitting = true;
                poofInstance.GetNode<Timer>("Life").Connect("timeout", this, "KillNode", new Godot.Collections.Array() { poofInstance });
                this.AddChild(poofInstance);

                Utils.globalPlayer.XyloNotes += currentHover.ToString();
            }

        }
        if (Input.IsActionJustPressed("closexylo"))
        {
            this.Exit();
        }
    }
    public void Exit()
    {

        Utils.TogglePause();
        Utils.KillNode(this);
        Utils.globalPlayer.isInstrumentOpen = false;

    }
    public void KillNode(Node node) => Utils.KillNode(node);
}
